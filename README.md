# Good Ol' MIUI Camera for Xiaomi's SDM845 family AOSP 13

### Cloning :
- Clone this repo in vendor/xiaomi/miuicamera in your working directory by :
```

git clone https://gitlab.com/bittencourtjulival/vendor_xiaomi_miuicamera.git -b miui vendor/xiaomi/miuicamera
```
Make these changes in **sdm845-common**

**sdm845.mk**
```
# MiuiCamera
$(call inherit-product, vendor/xiaomi/miuicamera/config.mk)
```

## Credits:
ItzDFPlayer
### https://t.me/itzdfplayer_stash <br>
Original repository:
https://gitlab.com/ItzDFPlayer/vendor_davinci-miuicamera
